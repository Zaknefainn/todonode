import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Subscription } from 'rxjs';

import { faPlusSquare } from '@fortawesome/free-solid-svg-icons';

import { AddFormService, AddTaskInterface } from './add-form.service';
import { ListService, ItemListInterface } from '../list/list.service';

@Component({
  selector: 'app-add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.scss']
})
export class AddFormComponent implements OnInit {
  faPlusSquare = faPlusSquare;
  adderForm: FormGroup;
  add$: Subscription;
  constructor(
    private addFormService: AddFormService,
    private fb: FormBuilder,
    private listService: ListService
  ) { }

  ngOnInit() {
    this.buildAdderForm();
  }

  buildAdderForm() {
    this.adderForm = this.fb.group({
      name: ['', [Validators.required]],
      author: ['']
    });
  }

  sendNewTask() {
    if (this.adderForm.valid) {
      const param = this.parametersForAddTask();
      this.listService.isLoading = true;

      this.add$ = this.addFormService.addNewTask(param).subscribe((res: ItemListInterface[]) => {
        this.listService.itemList = res;
        this.adderForm.controls.name.setValue('');
        this.listService.isLoading = false;
      });
    }
  }

  parametersForAddTask(): AddTaskInterface {
    const author = this.adderForm.get('author').value.toString();

    return {
      title: this.adderForm.get('name').value.toString(),
      author: author !== '' ? author : 'Anonymous',
    };
  }
}
