import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { ListService } from '../list/list.service';

export interface AddTaskInterface {
  readonly title: string;
  readonly author: string;
  readonly completed?: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AddFormService {
  readonly addUrl = '/api/tasks/add';

  constructor(
    private http: HttpClient,
    private listService: ListService
  ) { }

  public addNewTask(params: AddTaskInterface): Observable<object> {
    return this.http.post<object>(this.addUrl, params)
      .pipe(mergeMap(() => this.listService.getToDoList()));
  }
}
