import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TaskListComponent } from './task-list.component';

const secondaryRoutes: Routes = [
  { path: 'list', component: TaskListComponent },
];

@NgModule({
  imports: [
    RouterModule.forChild(secondaryRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TaskListRoutingModule { }
