import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../../shared/shared.module';
import { TaskListRoutingModule } from './task-list-routing.module';

import { TaskListComponent } from './task-list.component';
import { AddFormComponent } from './add-form/add-form.component';
import { ListComponent } from './list/list.component';
import { ItemComponent } from './list/item/item.component';

@NgModule({
  declarations: [
    TaskListComponent,
    AddFormComponent,
    ListComponent,
    ItemComponent,
  ],
  imports: [
    TaskListRoutingModule,
    CommonModule,
    SharedModule,
  ]
})
export class TaskListModule { }
