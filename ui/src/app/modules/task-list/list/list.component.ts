import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Subscription } from 'rxjs';

import { faTrashAlt, faTasks } from '@fortawesome/free-solid-svg-icons';

import { ListService, ItemListInterface } from './list.service';
import { ConfirmDialogComponent } from '../../../shared/confirm/confirm-dialog.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {
  faTrashAlt = faTrashAlt;
  faTasks = faTasks;
  list$: Subscription;
  dumpAll$: Subscription;

  constructor(
    public listService: ListService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.loadToDoList();
  }

  loadToDoList() {
    this.list$ = this.listService.getToDoList().subscribe((res: ItemListInterface[]) => {
      this.listService.itemList = res;
      this.listService.isLoading = false;
    });
  }

  dialogBeforeRemoveAll() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      width: '300px',
      data: {
        title: 'Remove all tasks ?',
        description: 'Would you like to remove all tasks from current list ?'
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed', result);
      if (result) {
        this.removeAllItemsFromList();
      }
    });
  }

  removeAllItemsFromList() {
    if (this.dumpAll$) {
      this.dumpAll$.unsubscribe();
    }

    this.dumpAll$ = this.listService.dumpList().subscribe(() => {
      this.listService.itemList = [];
      this.listService.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    if (this.list$) {
      this.list$.unsubscribe();
    }
    if (this.dumpAll$) {
      this.dumpAll$.unsubscribe();
    }
  }
}
