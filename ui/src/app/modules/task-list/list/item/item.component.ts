import { Component, OnInit, Input } from '@angular/core';
import { Subscription } from 'rxjs';
import { faTimes, faCircle, faCheckCircle, faPenSquare, faSpinner, faCalendarAlt, faUser } from '@fortawesome/free-solid-svg-icons';

import { ListService, ItemListInterface } from '../list.service';
import { ProgressBarService } from '../../../../core/progress-bar/progress-bar.service';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  updateRequest$: Subscription;
  faTimes = faTimes;
  faCircle = faCircle;
  faCheckCircle = faCheckCircle;
  faPenSquare = faPenSquare;
  faSpinner = faSpinner;
  faCalendarAlt = faCalendarAlt;
  faUser = faUser;

  @Input() listItemData: ItemListInterface;

  constructor(
    private progressBarService: ProgressBarService,
    private listService: ListService,
  ) { }

  ngOnInit() {
  }

  deleteTask(id: string) {
    this.progressBarService.progressBarVisible = true;

    this.listService.deleteTask(id).subscribe((res: ItemListInterface[]) => {
      this.listService.itemList = res;
      this.progressBarService.progressBarVisible = false;
    });
  }

  toggleStatus(status: boolean, id: string) {
    this.progressBarService.progressBarVisible = true;

    this.updateRequest$ = this.listService.updateTaskStatus({status, id}).subscribe(res => {
      this.listService.itemList = res;
      this.progressBarService.progressBarVisible = false;
    });
  }

  editDialog(): void {

  }
}
