import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { mergeMap } from 'rxjs/operators';

export interface ItemListInterface {
  readonly title: string;
  readonly completed: boolean;
  readonly id?: any;
  readonly author?: string;
  readonly createdTime?: Date;
  readonly updated_at?: Date;
}

export interface UpdateParametersInterface {
  readonly status: boolean;
  readonly id: string;
}

@Injectable({
  providedIn: 'root'
})
export class ListService {
  readonly getItemsUrl = '/api/tasks/';
  readonly removeItemUrl = '/api/tasks/remove';
  readonly updateStatusUrl = '/api/tasks/status';
  readonly dumpAllItemsUrl = '/api/tasks/dump';
  readonly itemList$ = new BehaviorSubject<ItemListInterface[]>([]);
  isLoadingStatus = true;

  constructor(private http: HttpClient) { }

  get itemList(): ItemListInterface[] {
    return this.itemList$.value;
  }

  set itemList(newItemList: ItemListInterface[]) {
    this.itemList$.next(newItemList);
  }

  get isLoading(): boolean {
    return this.isLoadingStatus;
  }

  set isLoading(newStatus: boolean) {
    this.isLoadingStatus = newStatus;
  }

  public getToDoList(): Observable<ItemListInterface[]> {
    return this.http.get<ItemListInterface[]>(this.getItemsUrl);
  }

  public deleteTask(id: string): Observable<object> {
    return this.http.delete<object>(`${this.removeItemUrl}/${id}`)
      .pipe(mergeMap(() => this.getToDoList()));
  }

  public updateTaskStatus(parameters: UpdateParametersInterface) {
    return this.http.patch<object>(`${this.updateStatusUrl}/${parameters.id}`, { status: !parameters.status })
      .pipe(mergeMap(() => this.getToDoList()));
  }

  public dumpList(): Observable<object> {
    return this.http.delete<object>(this.dumpAllItemsUrl);
  }
}
