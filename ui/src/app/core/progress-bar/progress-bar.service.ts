import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProgressBarService {
  isProgressBarVisible = false;

  get progressBarVisible() {
    return this.isProgressBarVisible;
  }

  set progressBarVisible(status: boolean) {
    this.isProgressBarVisible = status;
  }

  constructor() { }
}
