import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    ProgressBarComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    ProgressBarComponent
  ]
})
export class CoreModule { }
