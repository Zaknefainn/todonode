import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { mergeMap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};

@Injectable({
  providedIn: 'root'
})
export class AppService {
  readonly addUrl = '/api/todo/add';
  readonly updateStatusUrl = '/api/todo/update';
  readonly deleteUrl = '/api/todo/remove/';

  constructor(private http: HttpClient) {
  }

  // public getToDoList() {
  //   return this.http.get<Array<object>>(this.getItemsUrl);
  // }

  public updateTaskStatus(status: boolean, id: string) {
    return this.http.patch(`${this.updateStatusUrl}/${id}`, { status });
  }

  public deleteTaskStatus(id: string) {
    return this.http.delete(`${this.deleteUrl}/${id}`)
      .pipe(
        // mergeMap((res: Response) => this.getToDoList())
      );
  }
}
