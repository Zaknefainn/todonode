import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ui';
  toDoList: Array<object>;
  updateRequest$: Subscription;

  constructor(
    private appService: AppService
    ) {
  }

  ngOnInit() {
    // this.loadToDoList();
  }

  // loadToDoList() {
  //   this.appService.getToDoList().subscribe((res: Array<object>) => {
  //     this.toDoList = res;
  //   });
  // }

  // toggleStatus(data: boolean, id: string) {
  //   if (this.updateRequest$) {
  //     this.updateRequest$.unsubscribe();
  //   }

  //   this.updateRequest$ = this.appService.updateTaskStatus(data, id).subscribe();
  // }

  // deleteTask(id: string) {
  //   this.appService.deleteTaskStatus(id).subscribe(res => {
  //     this.toDoList = res;
  //   });
  // }
}
