import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons';

export interface DialogData {
  title: string;
  description: string;
}

@Component({
  selector: 'app-edit-dialog-dialog',
  templateUrl: 'confirm-dialog.component.html',
})
export class ConfirmDialogComponent {
  faExclamationTriangle = faExclamationTriangle;

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
