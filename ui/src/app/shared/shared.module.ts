import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatDialogModule } from '@angular/material/dialog';

import { PageNoFoundComponent } from './page-no-found/page-no-found.component';
import { ConfirmDialogComponent } from './confirm/confirm-dialog.component';

@NgModule({
  declarations: [PageNoFoundComponent, ConfirmDialogComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatDialogModule,
  ],
  exports: [
    CommonModule,
    MatButtonModule,
    MatListModule,
    MatProgressBarModule,
    PageNoFoundComponent,
    FormsModule,
    ReactiveFormsModule,
    FontAwesomeModule,
    MatDialogModule,
    ConfirmDialogComponent
  ],
  entryComponents: [
    ConfirmDialogComponent
  ]
})
export class SharedModule { }
