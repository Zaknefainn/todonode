require('dotenv').config();
const express = require('express');
const compression = require('compression')
const tasks = require('./app/routes/tasks-routes');
const db = require('./app/config/database');

const app = express();

db.authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch((error) => {
    console.log('Unable to connect to the database:', error);
  });

// * middleware 
app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(compression());

// * static
app.get('*.*', express.static('ui/dist/ui', {maxAge: '1y'}))

// * to index
app.all(/^((?!api).)*$/, (req, res) => {
  res.status(200).sendFile('/', {root: 'ui/dist/ui/'});
})

// * routes
app.use('/api/tasks', tasks);

app.listen(process.env.PORT, () => console.log(`App listening on port ${process.env.PORT}!`));