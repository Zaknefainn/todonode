const express = require('express');
const router = express.Router();
const taskController = require('../controllers/tasks-controller');

router.post('/add', taskController.addTask);

router.get('/', taskController.allTasks);

router.patch('/status/:id', taskController.toggleCompletedStatus);

router.delete('/remove/:id', taskController.removeTaskById);
router.delete('/dump', taskController.dumpAllTaskItems);

module.exports = router;