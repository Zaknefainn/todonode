const Sequelize = require('sequelize');
const sequelize = require('../config/database');
const Model = Sequelize.Model;

class Task extends Model {}
Task.init({
  title: {
    type: Sequelize.STRING,
    allowNull: false
  },
  completed: {
    type: Sequelize.TINYINT,
    defaultValue: 0,
    get() {
      const completed = this.getDataValue('completed');
      return completed === 0 ? false : true;
    },
    set(val) {
      const completedStatus = val === false ? 0 : 1;
      this.setDataValue('completed', completedStatus)
    }
  },
  author: {
    type: Sequelize.STRING,
    allowNull: false
  },
  created_at: {
    type: Sequelize.TIME,
    defaultValue: new Date(),
  },
  updated_at: {
    type: Sequelize.TIME,
    defaultValue: new Date(),
  }
}, {
  sequelize,
  modelName: 'task',
  timestamps: false
});

module.exports = Task;