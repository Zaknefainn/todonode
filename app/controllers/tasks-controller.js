const Task = require('../models/Task');

exports.addTask = async (req, res) => {
  try {

    await Task.create({
      title: req.body.title,
      author: req.body.author
    });

    res.json({ status: true });

  } catch (error) {

    res.sendStatus(500).json(error);
  }
}

exports.allTasks = async (req, res) => {
  try {
    
    const tasks = await Task.findAll();
    res.json(tasks);

  } catch(error) {

    res.sendStatus(500).json(error);
  }
}

exports.toggleCompletedStatus = async (req, res) => {
  try {

    await Task.update({
      completed: req.body.status,
      updated_at: new Date()
    }, {
      where: {
        id: req.params.id
      }
    });
    
    res.json({ status: true, message: 'status updated' });

  } catch (error) {

    res.sendStatus(500).json(error);
  }
}

exports.removeTaskById = async (req, res) => {
  try {

    await Task.destroy({
      where: {
        id: req.params.id
      }
    });

    res.json({ status: true, message: 'remove tasks' });
    
  } catch (error) {
    
    res.sendStatus(500).json(error);
  }
}

exports.dumpAllTaskItems = async (req, res) => {
  try {

    await Task.destroy({
      where: {},
      truncate: true
    });

    res.json({ status: true, message: 'task list dumped' });

  } catch (error) {
    res.sendStatus(500).json(error);
  }
}